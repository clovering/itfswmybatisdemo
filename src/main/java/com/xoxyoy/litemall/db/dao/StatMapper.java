package com.xoxyoy.litemall.db.dao;

import java.util.List;
import java.util.Map;

public interface StatMapper {
    //这里返回Map，所以不用像MBG生成的Mapper类那样，定义domain代码。
    List<Map> statUser();

    List<Map> statOrder();

    List<Map> statGoods();
}
