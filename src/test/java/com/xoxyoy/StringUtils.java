package com.xoxyoy;

import com.sun.istack.internal.Nullable;

/**
 * Created by jiangchao on 2018/12/21.
 */
public class StringUtils {
    public static boolean isEmpty(@Nullable Object str) {
        return str == null || "".equals(str);
    }
}
