package com.xoxyoy.liemall.db;

import com.xoxyoy.litemall.db.dao.StatMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class StatTest {
    private SqlSessionFactory sqlSessionFactory;
    private StatMapper statMapper;

    @Before
    public void setUp() throws Exception {
        // 创建sqlSessionFactory

        // mybatis配置文件
        String resource = "mybatis-config.xml";
        // 得到配置文件流
        InputStream inputStream = Resources.getResourceAsStream(resource);

        // 创建会话工厂，传入mybatis的配置文件信息
        sqlSessionFactory = new SqlSessionFactoryBuilder()
                .build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        statMapper = sqlSession.getMapper(StatMapper.class);
    }

    @Test
    public void statUser() {
        List<Map> map = statMapper.statUser();
        System.out.println(map.size());
    }

    @Test
    public void statOrder() {
        List<Map> map = statMapper.statOrder();
        System.out.println(map.size());
    }

    @Test
    public void statGoods() {
        List<Map> map = statMapper.statGoods();
        System.out.println(map.size());
    }
}
