package com.xoxyoy.liemall.db;

import com.xoxyoy.StringUtils;
import com.xoxyoy.litemall.db.dao.LitemallGoodsMapper;
import com.xoxyoy.litemall.db.domain.LitemallGoods;
import com.xoxyoy.litemall.db.domain.LitemallGoodsExample;
import com.xoxyoy.litemall.db.domain.LitemallGoods.Column;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

/**
 * Created by jiangchao on 2018/12/21.
 */
public class LitemallGoodsTest {
    private SqlSessionFactory sqlSessionFactory;
    private LitemallGoodsMapper goodsMapper;
    Column[] columns = new Column[]{Column.id, Column.name, Column.brief, Column.picUrl, Column.isHot, Column.isNew, Column.counterPrice, Column.retailPrice};

    // 此方法是在执行querySelective之前执行
    @Before
    public void setUp() throws Exception {
        // 创建sqlSessionFactory

        // mybatis配置文件
        String resource = "mybatis-config.xml";
        // 得到配置文件流
        InputStream inputStream = Resources.getResourceAsStream(resource);

        // 创建会话工厂，传入mybatis的配置文件信息
        sqlSessionFactory = new SqlSessionFactoryBuilder()
                .build(inputStream);
    }

    @Test
    public void querySelective() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        goodsMapper = sqlSession.getMapper(LitemallGoodsMapper.class);

        //Integer catId, Integer brandId, String keywords, Boolean isHot, Boolean isNew, Integer offset, Integer limit, String sort, String order
        LitemallGoodsExample example = new LitemallGoodsExample();
        LitemallGoodsExample.Criteria criteria1 = example.or();
        LitemallGoodsExample.Criteria criteria2 = example.or();

        int catId = 1008009;
        if (!StringUtils.isEmpty(catId) && catId != 0) {
            criteria1.andCategoryIdEqualTo(catId);
            criteria2.andCategoryIdEqualTo(catId);
        }
        int brandId = 0;
        if (!StringUtils.isEmpty(brandId)) {
            criteria1.andBrandIdEqualTo(brandId);
            criteria2.andBrandIdEqualTo(brandId);
        }
//        if (!StringUtils.isEmpty(isNew)) {
//            criteria1.andIsNewEqualTo(isNew);
//            criteria2.andIsNewEqualTo(isNew);
//        }
//        if (!StringUtils.isEmpty(isHot)) {
//            criteria1.andIsHotEqualTo(isHot);
//            criteria2.andIsHotEqualTo(isHot);
//        }
//        if (!StringUtils.isEmpty(keywords)) {
//            criteria1.andKeywordsLike("%" + keywords + "%");
//            criteria2.andNameLike("%" + keywords + "%");
//        }
        criteria1.andIsOnSaleEqualTo(true);
        criteria2.andIsOnSaleEqualTo(true);
        criteria1.andDeletedEqualTo(false);
        criteria2.andDeletedEqualTo(false);

        String sort = "add_time"; String order = "desc";
        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        List<LitemallGoods> list =  goodsMapper.selectByExample(example);
        System.out.println(list.size());
    }

    /**
     * 获取热卖商品
     *
     * @param offset
     * @param limit
     * @return
     */
    @Test
    public void queryByHot() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        goodsMapper = sqlSession.getMapper(LitemallGoodsMapper.class);

        LitemallGoodsExample example = new LitemallGoodsExample();
        example.or().andIsHotEqualTo(true).andIsOnSaleEqualTo(true).andDeletedEqualTo(false);
//        example.setOrderByClause("add_time desc");
        example.setOrderByClause(Column.addTime.desc());

        List<LitemallGoods> list = goodsMapper.selectByExampleSelective(example, columns);
        System.out.println(list.size());
    }

    /**
     * 获取新品上市
     *
     * @param offset
     * @param limit
     * @return
     */
    @Test
    public void queryByNew() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        goodsMapper = sqlSession.getMapper(LitemallGoodsMapper.class);

        LitemallGoodsExample example = new LitemallGoodsExample();
        example.or().andIsNewEqualTo(true).andIsOnSaleEqualTo(true).andDeletedEqualTo(false);
        example.setOrderByClause("add_time desc");
//        PageHelper.startPage(offset, limit);

        List<LitemallGoods> list = goodsMapper.selectByExampleSelective(example, columns);
        System.out.println(list.size());
    }
}
