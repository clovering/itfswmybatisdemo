package com.xoxyoy.liemall.db;

import com.xoxyoy.litemall.db.dao.LitemallAddressMapper;
import com.xoxyoy.litemall.db.domain.LitemallAddress;
import com.xoxyoy.litemall.db.domain.LitemallAddressExample;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

/**
 * Created by jiangchao on 2018/12/21.
 */
public class LitemallAddressTest {
    private SqlSessionFactory sqlSessionFactory;
    private LitemallAddressMapper addressMapper;

    // 此方法是在执行querySelective之前执行
    @Before
    public void setUp() throws Exception {
        // 创建sqlSessionFactory

        // mybatis配置文件
        String resource = "mybatis-config.xml";
        // 得到配置文件流
        InputStream inputStream = Resources.getResourceAsStream(resource);

        // 创建会话工厂，传入mybatis的配置文件信息
        sqlSessionFactory = new SqlSessionFactoryBuilder()
                .build(inputStream);
    }

    @Test
    public void querySelective() {
//        Integer userId; String name; Integer page; String sort; String order;
//        LitemallAddressExample example = new LitemallAddressExample();
//        LitemallAddressExample.Criteria criteria = example.createCriteria();

//        if (userId != null) {
//            criteria.andUserIdEqualTo(userId);
//        }
//        if (!StringUtils.isEmpty(name)) {
//            criteria.andNameLike("%" + name + "%");
//        }
//        criteria.andDeletedEqualTo(false);
//
//        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
//            example.setOrderByClause(sort + " " + order);
//        }

//        List<LitemallAddress> addresses = addressMapper.selectByExample(example);
//        System.out.println(addresses.size());



        SqlSession sqlSession = sqlSessionFactory.openSession();

        //创建LitemallAddressMapper对象，mybatis自动生成mapper代理对象
        addressMapper = sqlSession.getMapper(LitemallAddressMapper.class);

        LitemallAddressExample example = new LitemallAddressExample();
        LitemallAddressExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(false);
        List<LitemallAddress> addresses = addressMapper.selectByExample(example);
        System.out.println(addresses.size());

        //创建包装对象，设置查询条件
//        UserQueryVo userQueryVo = new UserQueryVo();
//        UserCustom userCustom = new UserCustom();
//        //由于这里使用动态sql，如果不设置某个值，条件不会拼接在sql中
//        userCustom.setSex("1");
//        //userCustom.setUsername("张三");
//
//        //传入多个id
//        List<Integer> ids = new ArrayList<Integer>();
//        ids.add(1);
//        ids.add(10);
//        ids.add(16);
//        //将ids通过userQueryVo传入statement中
//        userQueryVo.setIds(ids);
//
//        userQueryVo.setUserCustom(userCustom);
//        //调用userMapper的方法
//
//        List<UserCustom> list = userMapper.findUserList(userQueryVo);
//        //List<UserCustom> list = userMapper.findUserList(null);
//
//        System.out.println(list);
    }
}
