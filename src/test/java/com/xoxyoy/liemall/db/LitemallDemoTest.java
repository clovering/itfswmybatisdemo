package com.xoxyoy.liemall.db;

import com.xoxyoy.litemall.db.dao.LitemallDemoMapper;
import com.xoxyoy.litemall.db.domain.LitemallDemo;
import com.xoxyoy.litemall.db.domain.LitemallDemoExample;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

public class LitemallDemoTest {
    private SqlSessionFactory sqlSessionFactory;
    private LitemallDemoMapper litemallDemoMapper;

    @Before
    public void setUp() throws Exception {
        // 创建sqlSessionFactory

        // mybatis配置文件
        String resource = "mybatis-config.xml";
        // 得到配置文件流
        InputStream inputStream = Resources.getResourceAsStream(resource);

        // 创建会话工厂，传入mybatis的配置文件信息
        sqlSessionFactory = new SqlSessionFactoryBuilder()
                .build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        litemallDemoMapper = sqlSession.getMapper(LitemallDemoMapper.class);
    }

    @Test
    public void test() {
        LitemallDemoExample example = new LitemallDemoExample();
        List<LitemallDemo> litemallDemoList = litemallDemoMapper.selectByExample(example);
        System.out.println(litemallDemoList.size());
    }
}
